VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.UserControl XmlTreeView 
   ClientHeight    =   2445
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3210
   ScaleHeight     =   2445
   ScaleWidth      =   3210
   ToolboxBitmap   =   "XmlTreeView.ctx":0000
   Begin MSComctlLib.TreeView TreeView 
      Height          =   1695
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   2355
      _ExtentX        =   4154
      _ExtentY        =   2990
      _Version        =   393217
      HideSelection   =   0   'False
      Indentation     =   882
      LabelEdit       =   1
      LineStyle       =   1
      Style           =   6
      SingleSel       =   -1  'True
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "XmlTreeView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const STR_ALLOWED_NAME_CHARS As String = "abcdefghijklmnopqrstuvwxyz0123456789_"

Private objXmlDoc As New MSXML2.DOMDocument60
Attribute objXmlDoc.VB_VarHelpID = -1
Private strExclude As String
Private strCurrentKey As String
Private strTitleAttributes As String

Public Event MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Public Event MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
Public Event MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event KeyUp(KeyCode As Integer, Shift As Integer)
Public Event KeyPress(KeyAscii As Integer)
Public Event NodeClick(ByVal node As MSComctlLib.node)
Public Event Error(ByVal Code As Long, ByVal sDescription As String)
Public Event Refresh()
Public Event BeforeRemove(ByRef Cancel As Integer)
Public Event AfterRemove()
Public Event SelectionChanged()
Public Event OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
Public Event OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
Public Event Changed()
Public Event HitTest(x As Single, y As Single)

Private Sub UserControl_Initialize()

    TreeView.Left = 0
    TreeView.Top = 0
    TreeView.PathSeparator = "/"
    objXmlDoc.async = False

End Sub

Private Sub UserControl_Resize()

    TreeView.Width = UserControl.Width
    TreeView.Height = UserControl.Height

End Sub

Private Function GetNodeByKey(ByVal sKey As String) As MSXML2.IXMLDOMNode

    Set GetNodeByKey = objXmlDoc.selectSingleNode(sKey)

End Function

Private Sub TreeView_Collapse(ByVal node As MSComctlLib.node)

    If strCurrentKey <> SelectedKey Then
        RaiseEvent SelectionChanged

        strCurrentKey = SelectedKey
    End If

End Sub

Private Sub TreeView_Expand(ByVal node As MSComctlLib.node)

    If strCurrentKey <> SelectedKey Then
        RaiseEvent SelectionChanged
        
        strCurrentKey = SelectedKey
    End If

End Sub

Private Sub TreeView_KeyDown(KeyCode As Integer, Shift As Integer)

    RaiseEvent KeyDown(KeyCode, Shift)

End Sub

Private Sub TreeView_KeyPress(KeyAscii As Integer)

    RaiseEvent KeyPress(KeyAscii)

End Sub

Private Sub TreeView_KeyUp(KeyCode As Integer, Shift As Integer)

    If strCurrentKey <> SelectedKey Then
        RaiseEvent SelectionChanged

        strCurrentKey = SelectedKey
    End If

    RaiseEvent KeyUp(KeyCode, Shift)

End Sub

Private Sub TreeView_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)

    RaiseEvent MouseDown(Button, Shift, x, y)

End Sub

Private Sub TreeView_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

    RaiseEvent MouseMove(Button, Shift, x, y)

End Sub

Private Sub TreeView_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

    RaiseEvent MouseUp(Button, Shift, x, y)

    If strCurrentKey <> SelectedKey Then
        RaiseEvent SelectionChanged
        
        strCurrentKey = SelectedKey
    End If

End Sub

Private Sub TreeView_NodeClick(ByVal node As MSComctlLib.node)

    RaiseEvent NodeClick(node)

End Sub

Public Function HitTest(x As Single, y As Single) As node

    Set HitTest = TreeView.HitTest(x, y)

End Function

Public Function Load(ByVal sPath As String) As Boolean

    If objXmlDoc.Load(sPath) Then
        Load = True
        LoadTree Root
    Else
        Load = False
        
        RaiseEvent Error(objXmlDoc.parseError.errorCode, objXmlDoc.parseError.reason)
    End If

End Function

Public Function LoadXml(ByVal sXml As String) As Boolean

    Dim objXmlInstruction As MSXML2.IXMLDOMProcessingInstruction
    Dim cityAttr As MSXML2.IXMLDOMAttribute
    Dim rootNode As MSXML2.IXMLDOMNode

    If objXmlDoc.LoadXml(sXml) Then
        Set rootNode = objXmlDoc.childNodes(0)
        Set objXmlInstruction = objXmlDoc.createProcessingInstruction("xml", "version=""1.0"" encoding=""UTF-8""")
        Call objXmlDoc.insertBefore(objXmlInstruction, rootNode)

        Set cityAttr = rootNode.ownerDocument.createAttribute("ID")
        rootNode.Attributes.setNamedItem cityAttr
    
        Set cityAttr = rootNode.ownerDocument.createAttribute("Name")
        rootNode.Attributes.setNamedItem cityAttr
        
        Set cityAttr = rootNode.ownerDocument.createAttribute("Version")
        cityAttr.value = "0.1"
        rootNode.Attributes.setNamedItem cityAttr

        LoadXml = True
        LoadTree Root
    Else
        LoadXml = False
        
        RaiseEvent Error(objXmlDoc.parseError.errorCode, objXmlDoc.parseError.reason)
    End If

End Function

Public Sub Save(ByVal sFilename As String)

    objXmlDoc.Save sFilename

End Sub

Public Sub Refresh()

    Dim objNode As node, strKey As String
    
    strKey = Me.SelectedKey
    LoadTree Root

    RaiseEvent Refresh
    
    If strKey <> "" Then
        Set objNode = TreeView.Nodes.Item(strKey)
        If Not objNode Is Nothing Then
            objNode.Selected = True
            
            While Not objNode Is Nothing
                objNode.Expanded = True
                Set objNode = objNode.Parent
            Wend
        End If
    End If

End Sub

Public Sub AddLineNode()

    Dim lineAttr As MSXML2.IXMLDOMAttribute
    Dim lineElement As MSXML2.IXMLDOMElement
    
    Set lineElement = objXmlDoc.createElement("Line")
    
    Set lineAttr = objXmlDoc.createAttribute("Name")
    lineAttr.value = "����x����"
    lineElement.Attributes.setNamedItem lineAttr
    
    Set lineAttr = objXmlDoc.createAttribute("Status")
    lineAttr.value = "Running"
    lineElement.Attributes.setNamedItem lineAttr
    
    Set lineAttr = objXmlDoc.createAttribute("Description")
    lineElement.Attributes.setNamedItem lineAttr
    
    AddNode lineElement

End Sub

Public Sub AddStationNode()

    Dim stationAttr As MSXML2.IXMLDOMAttribute
    Dim stationElement As MSXML2.IXMLDOMElement, childElement As MSXML2.IXMLDOMElement
    
    Set stationElement = objXmlDoc.createElement("Station")
    
    Set stationAttr = objXmlDoc.createAttribute("Name")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Description")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Sequence")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Status")
    stationAttr.value = "Opened"
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Transfer")
    stationAttr.value = "False"
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Reversible")
    stationAttr.value = "True"
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Direction")
    stationAttr.value = "Right"
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Time")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Bus")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("BusStation")
    stationElement.Attributes.setNamedItem stationAttr
    
    Set stationAttr = objXmlDoc.createAttribute("Surround")
    stationElement.Attributes.setNamedItem stationAttr
    
    AddNode stationElement

End Sub

Private Sub AddNode(ByRef vData As Variant)

    Dim strKey As String, objNode As MSXML2.IXMLDOMNode, objTempElement As MSXML2.IXMLDOMElement, objAttr As MSXML2.IXMLDOMAttribute
    Dim treeNode As node
    
    If SelectedNode Is Nothing Then Exit Sub
    
On Error Resume Next
    
    If IsObject(vData) Then
        Set objNode = SelectedNode.appendChild(vData.cloneNode(True))
    Else
        Set objTempElement = objXmlDoc.createElement(vData)
    End If
    
    strKey = SelectedKey & "/*[" & SelectedNode.childNodes.Length & "]"
    Set treeNode = TreeView.Nodes.Add(SelectedKey, tvwChild, strKey, objNode.nodeName)

    treeNode.EnsureVisible
    
    If objNode.hasChildNodes Then LoadTree objNode, strKey
    
    If Err.Number > 0 Then
        RaiseEvent Error(Err.Number, Err.Description)
    Else
        RaiseEvent Changed
    End If
    
End Sub

Public Sub Remove()

    Dim intCancel As Integer
    Dim objNodeParent As MSXML2.IXMLDOMNode
    Dim objTreeParent As node
    
    If SelectedNode Is Nothing Then Exit Sub
    
    Set objNodeParent = SelectedNode.parentNode
    Set objTreeParent = TreeView.SelectedItem.Parent
     
    If objNodeParent Is Nothing Or objTreeParent Is Nothing Then Exit Sub
    
    RaiseEvent BeforeRemove(intCancel)
    
    If intCancel <> 0 Then Exit Sub
    
    SelectedNode.parentNode.removeChild SelectedNode
    
    While objTreeParent.Children > 0
         TreeView.Nodes.Remove objTreeParent.Child.Key
    Wend
    
    LoadTree objNodeParent, objTreeParent.Key
    
    RaiseEvent AfterRemove
    RaiseEvent SelectionChanged
    RaiseEvent Changed

End Sub

Public Property Get Xml() As String

    If Not Root Is Nothing Then Xml = Root.Xml

End Property

Public Static Property Get Exclude() As String

    Exclude = strExclude

End Property

Public Property Let Exclude(ByVal value As String)

    strExclude = value

End Property

Public Property Get TitleAttributes() As String

    TitleAttributes = strTitleAttributes

End Property

Public Static Property Let TitleAttributes(ByVal value As String)

    strTitleAttributes = value

End Property

Public Property Let Root(ByRef value As MSXML2.IXMLDOMNode)

    If Not LoadXml(value.Xml) Then RaiseEvent Error(objXmlDoc.parseError.errorCode, objXmlDoc.parseError.reason)

End Property

Public Property Get Root() As MSXML2.IXMLDOMNode

    Set Root = objXmlDoc.documentElement

End Property

Public Property Get SelectedNode() As MSXML2.IXMLDOMNode

    If Not TreeView.SelectedItem Is Nothing Then Set SelectedNode = objXmlDoc.selectSingleNode(TreeView.SelectedItem.Key)

End Property

Public Property Get SelectedKey() As String

    If Not TreeView.SelectedItem Is Nothing Then SelectedKey = TreeView.SelectedItem.Key

End Property

Private Sub LoadTree(ByRef oNode As MSXML2.IXMLDOMNode, Optional ByVal sParent As String = "")

    Dim intCount As Integer, intCount2 As Integer
    Dim objNode As MSXML2.IXMLDOMNode
    Dim strPath As String
    Dim objNodeList As MSXML2.IXMLDOMNodeList
    Dim strName As String
    
    If oNode Is Nothing Then Exit Sub
    
    If sParent = "" Then
        sParent = "*[1]"
        strCurrentKey = "*[1]"
        TreeView.Nodes.Clear
        
        strName = oNode.nodeName
        Set objNodeList = oNode.selectNodes("@*[contains('ID',name())]")
        If objNodeList.Length > 0 Then strName = strName & " (" & objNodeList.Item(0).nodeValue & ")"
        Set objNodeList = Nothing
        
        TreeView.Nodes.Add , , sParent, strName
        TreeView.SelectedItem = TreeView.Nodes.Item(sParent)
    End If

    For intCount = 0 To oNode.childNodes.Length - 1
        Set objNode = oNode.childNodes(intCount)
        If InStr(1, Exclude, objNode.nodeName & ";", vbTextCompare) = 0 Then
            strPath = sParent & "/*[" & intCount + 1 & "]"
            strName = objNode.nodeName
            
            If Len(strTitleAttributes) > 0 Then
                Set objNodeList = objNode.selectNodes("@*[contains('" & strTitleAttributes & "',name())]")
                If objNodeList.Length > 0 Then
                    strName = strName & " ("
                    For intCount2 = 0 To objNodeList.Length - 1
                        strName = strName & objNodeList.Item(intCount2).nodeValue
                        If intCount2 < objNodeList.Length - 1 Then
                            strName = strName & ", "
                        End If
                    Next
                    strName = strName & ")"
                End If
                Set objNodeList = Nothing
            End If
            
            TreeView.Nodes.Add sParent, tvwChild, strPath, strName
        
            If objNode.hasChildNodes Then
                Call LoadTree(objNode, strPath)
            End If
        End If
    Next
End Sub
