VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmMain 
   Caption         =   "MetroX XML Editor"
   ClientHeight    =   6135
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   11880
   BeginProperty Font 
      Name            =   "����"
      Size            =   10.5
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   NegotiateMenus  =   0   'False
   ScaleHeight     =   6135
   ScaleWidth      =   11880
   StartUpPosition =   3  '����ȱʡ
   Begin MetroXXMLEditor.Splitter Splitter 
      Height          =   1095
      Left            =   1440
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1200
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   1931
      SplitterPosition=   30
      SplitterWidth   =   60
      Begin MetroXXMLEditor.XmlTreeView XmlView 
         Height          =   495
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   675
         _ExtentX        =   1191
         _ExtentY        =   873
      End
      Begin MetroXXMLEditor.XmlAttributeTab XmlAttribute 
         Height          =   555
         Left            =   960
         TabIndex        =   2
         Top             =   240
         Width           =   555
         _ExtentX        =   979
         _ExtentY        =   979
      End
   End
   Begin MSComctlLib.StatusBar StatusBar 
      Align           =   2  'Align Bottom
      Height          =   420
      Left            =   0
      TabIndex        =   3
      Top             =   5715
      Width           =   11880
      _ExtentX        =   20955
      _ExtentY        =   741
      Style           =   1
      ShowTips        =   0   'False
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Bevel           =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileNew 
         Caption         =   "&New"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuFileSep01 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open"
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuFileSep02 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save"
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileSep03 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuXML 
      Caption         =   "XML"
      Visible         =   0   'False
      Begin VB.Menu mnuXMLInsertLine 
         Caption         =   "Insert Line"
      End
      Begin VB.Menu mnuXMLInsertStation 
         Caption         =   "Insert Station"
      End
      Begin VB.Menu mnuXMLSep01 
         Caption         =   "-"
      End
      Begin VB.Menu mnuXMLDelete 
         Caption         =   "Delete Node"
      End
      Begin VB.Menu mnuXMLSep02 
         Caption         =   "-"
      End
      Begin VB.Menu mnuXMLRefresh 
         Caption         =   "&Refresh"
      End
   End
   Begin VB.Menu mnuSourceCode 
      Caption         =   "Souce&code"
      Begin VB.Menu mnuSourceCodeRoot 
         Caption         =   "Roo&t"
      End
      Begin VB.Menu mnuSourceCodeSep01 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSourceCodeNode 
         Caption         =   "Current N&ode"
      End
      Begin VB.Menu mnuSourceCodeSep02 
         Caption         =   "-"
      End
      Begin VB.Menu mnuSourceCodeCopyToClipboard 
         Caption         =   "Copy to Clipboard"
         Checked         =   -1  'True
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpAttrIntro 
         Caption         =   "Attribute Introduction"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "About"
      End
      Begin VB.Menu mnuHelpSep01 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpTopmost 
         Caption         =   "Topmost window"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private objXmlDoc As New MSXML2.DOMDocument60
Private objNode As MSXML2.IXMLDOMNode
Private strFilename As String
Private blnChanged As Boolean
Private objCommonDialog As New clsCommonDialog

Private Sub Form_Initialize()

    If App.PrevInstance Then End

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)

    If (KeyCode = vbKeyF5) Then
        KeyCode = 0
        
        Call XmlView.Refresh
    End If
    
End Sub

Private Sub Form_Load()

    Me.Caption = App.Title & " - [" & App.Major & "." & App.Minor & "." & App.Revision & "]"
    mnuXMLRefresh.Caption = "Refresh" & vbTab & "F5"
    
    Set Splitter.LeftOrTopControl = XmlView
    Set Splitter.RightOrBottomControl = XmlAttribute
    
    'Call FormAllwaysOnTop(Me)
    'Call Form_Resize

    If (Not inIDE()) Then Call StartResizeMinMax(Me, 400, 1280, 300, 768)

    XmlView.TitleAttributes = "Sequence,Name"
    NewDocument

End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Dim Result As VbMsgBoxResult

    If blnChanged Then
        Result = MsgBox("Do you want to save changes?", vbQuestion + vbYesNoCancel, App.Title)
        
        If (Result = vbYes) Then
            Call SaveDocument
        ElseIf (Result = vbCancel) Then
            Cancel = 1
        End If
    End If

End Sub

Private Sub Form_Resize()

    If (WindowState <> 1) Then
        Splitter.Move 30, 30, Me.ScaleWidth - 30, Me.ScaleHeight - StatusBar.Height - 30
    End If

End Sub

Private Sub mnuFileExit_Click()

    Unload Me

End Sub

Private Sub mnuFileNew_Click()

    Call NewDocument

End Sub

Private Sub mnuFileOpen_Click()

    Call OpenDocument

End Sub

Private Sub mnuFileSave_Click()

    Call SaveDocument

End Sub

Private Sub InvokeNotepad(ByVal isRoot As Boolean)

    Dim strKeys As String, intCount As Integer
    
On Error Resume Next
    
    If (isRoot) Then
        strKeys = XmlView.Root.Xml
    Else
        strKeys = XmlView.SelectedNode.Xml
    End If
    
    If (mnuSourceCodeCopyToClipboard.Checked) Then
        Call ClipboardSetText(Me.hWnd, strKeys)
    Else
        Shell "notepad.exe", vbNormalFocus
        
        For intCount = 1 To Len(strKeys)
            SendKeys Mid(strKeys, intCount, 1), False
        Next
        
        Me.WindowState = vbMinimized
    End If
    
End Sub

Private Sub NewDocument()

    Dim Result As VbMsgBoxResult

    If blnChanged Then
        Result = MsgBox("Do you want to save changes?", vbQuestion + vbYesNoCancel, App.Title)
        
        If (Result = vbYes) Then
            Call SaveDocument
        ElseIf (Result = vbCancel) Then
            Exit Sub
        End If
    End If
    
    XmlView.LoadXml "<City />"
    XmlAttribute.node = XmlView.Root
    XmlView_SelectionChanged
    
    strFilename = ""
    blnChanged = False
    
End Sub

Private Sub OpenDocument()

    Dim Result As VbMsgBoxResult

    If blnChanged Then
        Result = MsgBox("Do you want to save changes?", vbQuestion + vbYesNoCancel, App.Title)
        
        If (Result = vbYes) Then
            If (Not SaveDocument) Then Exit Sub
        ElseIf (Result = vbCancel) Then
            Exit Sub
        End If
    End If
    
    Call objCommonDialog.VBGetOpenFileName(strFilename, , , , , True, "main.xml| main.xml", , strFilename, "Open main.xml file")

    If strFilename <> "" Then
        XmlView.Load strFilename
        blnChanged = False
    End If
    
End Sub

Private Function SaveDocument() As Boolean

    SaveDocument = False
        
    If (strFilename = "") Then
        strFilename = "main.xml"
        Call objCommonDialog.VBGetSaveFileName(strFilename, , , "main.xml| main.xml", , , "Save main.xml file", "xml")
    End If

    If strFilename <> "" Then
        XmlView.Save strFilename
        blnChanged = False
        SaveDocument = True
    End If
    
End Function

Private Sub DeleteNode()

    If MsgBox("You are about to delete a node." & vbCrLf & "Do you want to continue?", vbQuestion + vbYesNo + vbDefaultButton2) = vbYes Then XmlView.Remove

End Sub

Private Sub mnuHelpAbout_Click()

    Dim aboutString As String

    aboutString = "Project: MetroX XML Editor" & vbNewLine & vbNewLine
    aboutString = aboutString & "Description" & vbNewLine
    aboutString = aboutString & vbTab & "This is a part of the project MetroX, which is used to generate data files for it." & vbNewLine & vbNewLine
    aboutString = aboutString & "About Me" & vbNewLine
    aboutString = aboutString & vbTab & "Call MsgBox(""Written by Walkline Wang."")"
    
    Call MsgBox(aboutString, vbInformation, "About")
    
End Sub

Private Sub mnuHelpAttrIntro_Click()

    Dim introString As String

    introString = "Attribute Introduction" & vbNewLine & vbNewLine
    introString = introString & "[City]:" & vbNewLine
    introString = introString & vbTab & "[ID]: Data package identify code" & vbNewLine
    introString = introString & vbTab & "[Name]: A well-known city name" & vbNewLine
    introString = introString & vbTab & "[Version]: Data package version" & vbNewLine & vbNewLine
    introString = introString & "[Line]:" & vbNewLine
    introString = introString & vbTab & "[Name]: A line name" & vbNewLine
    introString = introString & vbTab & "[Status]: Status of this line" & vbNewLine
    introString = introString & vbTab & "[Description]: A short introduce of this line" & vbNewLine & vbNewLine
    introString = introString & "[Station]:" & vbNewLine
    introString = introString & vbTab & "[Name]: A station name" & vbNewLine
    introString = introString & vbTab & "[Description]: A short introduce of this station" & vbNewLine
    introString = introString & vbTab & "[Sequence]: A sequence number of this station" & vbNewLine
    introString = introString & vbTab & "[Status]: Indicate whether opened or closed" & vbNewLine
    introString = introString & vbTab & "[Transfer]: Indicate whether the transfer station" & vbNewLine
    introString = introString & vbTab & "[Reversible]: Indicate whether you can reach the other sides" & vbNewLine
    introString = introString & vbTab & "[Direction]: Indicate which side door will be opened" & vbNewLine
    introString = introString & vbTab & "[Time]: Time period of the train running" & vbNewLine
    introString = introString & vbTab & "[Bus]: Bus lines around station" & vbNewLine
    introString = introString & vbTab & "[BusStation]: Bus stations around station" & vbNewLine
    introString = introString & vbTab & "[Surround]: Buildings, famous road names and landmarks around station"
    
    Call MsgBox(introString, vbInformation, App.Title)

End Sub

Private Sub mnuHelpTopmost_Click()

    mnuHelpTopmost.Checked = Not mnuHelpTopmost.Checked
    
    Call FormAllwaysOnTop(Me, mnuHelpTopmost.Checked)

End Sub

Private Sub mnuSourceCodeCopyToClipboard_Click()

    mnuSourceCodeCopyToClipboard.Checked = Not mnuSourceCodeCopyToClipboard.Checked

End Sub

Private Sub mnuSourceCodeNode_Click()

    Call InvokeNotepad(False)

End Sub

Private Sub mnuSourceCodeRoot_Click()

    Call InvokeNotepad(True)

End Sub

Private Sub mnuXMLDelete_Click()

    Call DeleteNode

End Sub

Private Sub mnuXMLInsertLine_Click()

    XmlView.AddLineNode
    XmlView.Refresh

End Sub

Private Sub mnuXMLInsertStation_Click()

    XmlView.AddStationNode
    XmlView.Refresh

End Sub

Private Sub mnuXMLRefresh_Click()

    XmlView.Refresh
            
End Sub

Private Sub XmlAttribute_Changed()

    blnChanged = True

End Sub

Private Sub XmlAttribute_Error(ByVal iNumber As Integer, sDescription As String)

    MsgBox sDescription
    
End Sub

Private Sub XmlView_Changed()

    blnChanged = True
    
End Sub

Private Sub XmlView_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

    If (Button <> vbKeyRButton) Then Exit Sub

    Dim currentNode As node
    Set currentNode = XmlView.HitTest(x, y)
    
    If (currentNode Is Nothing) Then
        mnuXMLInsertLine.Enabled = False
        mnuXMLInsertStation.Enabled = False
        mnuXMLDelete.Enabled = False
    Else
        If (Left(currentNode.Text, 4) = "Line") Then
            mnuXMLInsertLine.Enabled = False
            mnuXMLInsertStation.Enabled = True
            mnuXMLDelete.Enabled = True
        ElseIf (Left(currentNode.Text, 4) = "City") Then
            mnuXMLInsertLine.Enabled = True
            mnuXMLInsertStation.Enabled = False
            mnuXMLDelete.Enabled = False
        ElseIf (Left(currentNode.Text, 7) = "Station") Then
            mnuXMLInsertLine.Enabled = False
            mnuXMLInsertStation.Enabled = False
            mnuXMLDelete.Enabled = True
        End If
    End If

    PopupMenu mnuXML
        
End Sub

Private Sub XmlView_SelectionChanged()

    If Not XmlView.SelectedNode Is Nothing Then
        XmlAttribute.node = XmlView.SelectedNode
        StatusBar.SimpleText = "Node key: " & XmlView.SelectedKey
    End If
    
End Sub
