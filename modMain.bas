Attribute VB_Name = "modMain"
Option Explicit
    
Private Const CF_TEXT = 1
Private Const CF_UNICODETEXT = 13
Private Const GHND = &H42&

Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOSIZE = &H1

Public Const CityIDArray = "022Tianjin,010Beijing,021Shanghai,023Chongqing,028Chengdu"
Public Const CityNameArray = "天津,北京,上海,重庆,成都"

Public Const LineStatusArray = "Running,Planning,Constructing"

Public Const StationStatusArray = "Opened,Closed"
Public Const StationTransferArray = "True,False"
Public Const StationReversibleArray = "True,False"
Public Const StationDirectionArray = "Left,Right"

Public Declare Function OpenClipboard Lib "user32" (ByVal hWnd As Long) As Long
Public Declare Function SetClipboardData Lib "user32" (ByVal Format As Long, ByVal hMem As Long) As Long
Public Declare Function CloseClipboard Lib "user32" () As Long
Public Declare Function EmptyClipboard Lib "user32" () As Long
Public Declare Function GlobalAlloc Lib "kernel32" (ByVal flags As Long, ByVal Length As Long) As Long
Public Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Public Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Public Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal pDest As Long, ByVal pSource As Long, ByVal Length As Long)
Public Declare Function SetWindowPos Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long

Public Function inIDE() As Boolean

    On Error GoTo ErrHanele

    inIDE = True
    Debug.Print 1 / 0   '触发错误,编译后这一行不会执行.
    inIDE = False
    
    Exit Function
    
ErrHanele:

End Function

Public Sub FormAllwaysOnTop(F As Form, Optional TopMost As Boolean = True)

    Dim hWnd As Long

    hWnd = F.hWnd

    If TopMost Then
        Call SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW Or SWP_NOMOVE Or SWP_NOSIZE)
    Else
        Call SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, 0, 0, SWP_SHOWWINDOW Or SWP_NOMOVE Or SWP_NOSIZE)
    End If

End Sub

Public Function ClipboardSetText(ByVal hWnd As Long, ByVal sText$) As Boolean

    Dim hMem As Long, pMem As Long, s As String
    Dim bOk As Boolean
    
    If OpenClipboard(hWnd) = 0 Then Exit Function
    EmptyClipboard
    
    ' ANSI
    's = StrConv(sText, vbFromUnicode)
    'hMem = GlobalAlloc(GHND, LenB(s) + 1)
    'pMem = GlobalLock(hMem)
    'CopyMemory ByVal pMem, ByVal StrPtr(s), LenB(s)
    'GlobalUnlock hMem
    'bOk = SetClipboardData(CF_TEXT, hMem) <> 0
    'If Not bOk Then GlobalFree hMem
    
    ' UNICODE:
    hMem = GlobalAlloc(GHND, LenB(sText) + 2)
    pMem = GlobalLock(hMem)
    CopyMemory ByVal pMem, ByVal StrPtr(sText), LenB(sText)
    GlobalUnlock hMem
    bOk = SetClipboardData(CF_UNICODETEXT, hMem) <> 0
    If Not bOk Then GlobalFree hMem
    
    CloseClipboard
    ClipboardSetText = bOk
    
End Function
