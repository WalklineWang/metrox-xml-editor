VERSION 5.00
Begin VB.Form frmBusImport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Bus Import Tool"
   ClientHeight    =   5055
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   2880
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5055
   ScaleWidth      =   2880
   StartUpPosition =   3  '窗口缺省
   Begin VB.CommandButton cmdImport 
      Caption         =   "Import"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   120
      TabIndex        =   3
      Top             =   4560
      Width           =   2655
   End
   Begin VB.ListBox lstBusLines 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3840
      Left            =   120
      Sorted          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   600
      Width           =   2655
   End
   Begin VB.TextBox txtBusLine 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      TabIndex        =   1
      Top             =   120
      Width           =   1695
   End
   Begin VB.Label lblBusLine 
      AutoSize        =   -1  'True
      Caption         =   "Bus line:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   120
      TabIndex        =   0
      Top             =   172
      Width           =   810
   End
End
Attribute VB_Name = "frmBusImport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private busString As String

Private Sub cmdImport_Click()

    Unload Me

End Sub

Private Sub Form_Activate()

    txtBusLine.SetFocus

End Sub

Private Sub Form_Load()

    Call FormAllwaysOnTop(Me)

End Sub

Public Sub setString(bus As String)

    Dim iCount As Integer, busArray() As String

    If (Trim(bus) = "") Then Exit Sub

    busString = bus
    busArray = Split(bus, "::")
    
    If (UBound(busArray) > 0) Then
        For iCount = 0 To UBound(busArray)
            lstBusLines.AddItem busArray(iCount)
        Next
    End If
    
End Sub

Public Function getString() As String

    getString = busString

End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)

    Dim iCount As Integer

    Select Case UnloadMode
        Case vbFormControlMenu    '用户从窗体上的“控件”菜单中选择“关闭”指令。
            '
        Case vbFormCode           'Unload 语句被代码调用。
            For iCount = 0 To lstBusLines.ListCount
                busString = busString & lstBusLines.List(iCount)
                
                If iCount < lstBusLines.ListCount - 1 Then busString = busString & "::"
            Next
        Case vbAppWindows         '当前 Microsoft Windows 操作环境会话结束。
            '
        Case vbAppTaskManager     'Microsoft Windows 任务管理器正在关闭应用程序。
            '
        Case vbFormMDIForm        'MDI 子窗体正在关闭，因为 MDI 窗体正在关闭。
            '
        Case vbFormOwner          '因为窗体的所有者正在关闭，所以窗体也在关闭。
            '
    End Select

End Sub

Private Sub lstBusLines_DblClick()

    If (lstBusLines.ListIndex >= 0) Then lstBusLines.RemoveItem lstBusLines.ListIndex

End Sub

Private Sub txtBusLine_KeyDown(KeyCode As Integer, Shift As Integer)

    If (KeyCode = vbKeyReturn) Then
        KeyCode = 0
        
        If (Trim(txtBusLine.Text) <> "") Then
            If (Not isDuplicate) Then
                lstBusLines.AddItem txtBusLine.Text
            Else
                Call MsgBox("Duplicated items!", vbExclamation, App.Title)
            End If
        Else
            Call MsgBox("Bus number could not be empty!", vbExclamation, App.Title)
        End If
        
        txtBusLine.Text = ""
        txtBusLine.SetFocus
    End If

End Sub

Private Function isDuplicate() As Boolean

    Dim iCount As Integer
    Dim checkString As String

    isDuplicate = False
    checkString = txtBusLine.Text
    
    For iCount = 0 To lstBusLines.ListCount
        If (lstBusLines.List(iCount) = checkString) Then
            isDuplicate = True
            
            Exit For
        End If
    Next
            
End Function

