VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.UserControl XmlAttributeTab 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   Begin RichTextLib.RichTextBox txtValue 
      Height          =   975
      Left            =   420
      TabIndex        =   1
      Top             =   480
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   1720
      _Version        =   393217
      BorderStyle     =   0
      Enabled         =   -1  'True
      DisableNoScroll =   -1  'True
      OLEDragMode     =   0
      OLEDropMode     =   0
      TextRTF         =   $"XmlAttributeTab.ctx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.TabStrip TabStrip 
      Height          =   1875
      Left            =   60
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   60
      Width           =   3795
      _ExtentX        =   6694
      _ExtentY        =   3307
      ShowTips        =   0   'False
      HotTracking     =   -1  'True
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   1
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            ImageVarType    =   2
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Menu mnuPopup 
      Caption         =   "Popup"
      Begin VB.Menu mnuPopupCity 
         Caption         =   "City"
         Begin VB.Menu mnuPopupCityID 
            Caption         =   "ID"
            Begin VB.Menu mnuPopupCityIDItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
         Begin VB.Menu mnuPopupCityName 
            Caption         =   "Name"
            Begin VB.Menu mnuPopupCityNameItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
      End
      Begin VB.Menu mnuPopupLine 
         Caption         =   "Line"
         Begin VB.Menu mnuPopupLineStatus 
            Caption         =   "Status"
            Begin VB.Menu mnuPopupLineStatusItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
      End
      Begin VB.Menu mnuPopupStation 
         Caption         =   "Station"
         Begin VB.Menu mnuPopupStationStatus 
            Caption         =   "Status"
            Begin VB.Menu mnuPopupStationStatusItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
         Begin VB.Menu mnuPopupStationTransfer 
            Caption         =   "Transfer"
            Begin VB.Menu mnuPopupStationTransferItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
         Begin VB.Menu mnuPopupStationReversible 
            Caption         =   "Reversible"
            Begin VB.Menu mnuPopupStationReversibleItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
         Begin VB.Menu mnuPopupStationDirection 
            Caption         =   "Direction"
            Begin VB.Menu mnuPopupStationDirectionItems 
               Caption         =   "(Empty)"
               Enabled         =   0   'False
               Index           =   0
            End
         End
         Begin VB.Menu mnuPopupStationSep01 
            Caption         =   "-"
         End
         Begin VB.Menu mnuPopupStationBusImportTool 
            Caption         =   "Bus Import Tool"
         End
      End
   End
End
Attribute VB_Name = "XmlAttributeTab"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private objNode As MSXML2.IXMLDOMNode
Private objAttr As MSXML2.IXMLDOMAttribute

Public Event Changed()
Public Event Error(ByVal iNumber As Integer, sDescription As String)
Public Event KeyDown(KeyCode As Integer, Shift As Integer)
Public Event KeyPress(KeyAscii As Integer)
Public Event KeyUp(KeyCode As Integer, Shift As Integer)

Private CityIDs() As String, CityNames() As String
Private LineStatuses() As String
Private StationStatuses() As String, StationTransfers() As String, StationReversibles() As String, StationDirections() As String

Public Property Get node() As MSXML2.IXMLDOMNode

    Set node = objNode

End Property

Public Property Get Selected() As MSXML2.IXMLDOMAttribute

    If Not objNode Is Nothing And Not TabStrip.SelectedItem Is Nothing Then
        Set Selected = objNode.Attributes.getNamedItem(TabStrip.SelectedItem)
    Else
        Set Selected = Nothing
    End If

End Property

Public Property Let node(ByRef value As MSXML2.IXMLDOMNode)

    Dim intCount As Integer
    Dim objText As TextBox
    Dim objTab As MSComctlLib.Tab
    Dim strName As String, strValue As String
    
    Set objNode = value
    TabStrip.Tabs.Clear
    
    If Not objNode Is Nothing Then
        For intCount = 1 To objNode.Attributes.Length
            strName = objNode.Attributes(intCount - 1).nodeName
            TabStrip.Tabs.Add intCount, strName, strName
        Next
    End If

    Call ResetTextBox

    If Not Selected Is Nothing Then
        txtValue.Text = Selected.nodeValue
    End If
    
End Property

Private Sub mnuPopupCityIDItems_Click(Index As Integer)

    txtValue.Text = CityIDs(Index)
    
    TabStrip.Tabs.Item(2).Selected = True
    txtValue.Text = CityNames(Index)

End Sub

Private Sub mnuPopupCityNameItems_Click(Index As Integer)

    txtValue.Text = CityNames(Index)
    
    TabStrip.Tabs.Item(1).Selected = True
    txtValue.Text = CityIDs(Index)

End Sub

Private Sub mnuPopupLineStatusItems_Click(Index As Integer)

    txtValue.Text = LineStatuses(Index)
    
End Sub

Private Sub mnuPopupStationBusImportTool_Click()

    Dim ImportTool As New frmBusImport

    ImportTool.setString (txtValue.Text)
    ImportTool.Show 'vbModal
    
    txtValue.Text = ImportTool.getString
    Unload ImportTool
    
End Sub

Private Sub mnuPopupStationDirectionItems_Click(Index As Integer)

    txtValue.Text = StationDirections(Index)

End Sub

Private Sub mnuPopupStationReversibleItems_Click(Index As Integer)

    txtValue.Text = StationReversibles(Index)

End Sub

Private Sub mnuPopupStationStatusItems_Click(Index As Integer)

    txtValue.Text = StationStatuses(Index)

End Sub

Private Sub mnuPopupStationTransferItems_Click(Index As Integer)

    txtValue.Text = StationTransfers(Index)

End Sub

Private Sub TabStrip_Click()

    Dim iCount As Integer

    For iCount = 1 To TabStrip.Tabs.Count
        TabStrip.Tabs.Item(iCount).HighLighted = False
    Next

    If Not Selected Is Nothing Then
        TabStrip.SelectedItem.HighLighted = True
        
        txtValue.Text = Selected.nodeValue
        txtValue.SetFocus
    End If

End Sub

Private Sub TabStrip_KeyDown(KeyCode As Integer, Shift As Integer)

    RaiseEvent KeyDown(KeyCode, Shift)

End Sub

Private Sub TabStrip_KeyPress(KeyAscii As Integer)

    RaiseEvent KeyPress(KeyAscii)

End Sub

Private Sub TabStrip_KeyUp(KeyCode As Integer, Shift As Integer)

    RaiseEvent KeyUp(KeyCode, Shift)

End Sub

Private Sub Add(ByVal cityAttribute As MSXML2.IXMLDOMAttribute)

    objNode.Attributes.setNamedItem cityAttribute
    
    If Err.Number > 0 Then
        RaiseEvent Error(Err.Number, Err.Description)
    Else
        RaiseEvent Changed

        TabStrip.Tabs.Add , cityAttribute.Text, cityAttribute.Text
        Call ResetTextBox
    End If
    
End Sub

Public Sub Remove()

On Error Resume Next

    objNode.Attributes.removeNamedItem TabStrip.SelectedItem

    If Err.Number > 0 Then
        RaiseEvent Error(Err.Number, Err.Description)
    Else
        TabStrip.Tabs.Remove TabStrip.SelectedItem.Index
        
        RaiseEvent Changed
    End If

    Call ResetTextBox

End Sub

Private Sub txtValue_Change()

On Error Resume Next

    If Not Selected Is Nothing Then
        Selected.nodeValue = txtValue.Text
    End If
    
    If Err.Number > 0 Then
        RaiseEvent Error(Err.Number, Err.Description)
    Else
        RaiseEvent Changed
    End If

End Sub

Private Sub txtValue_KeyDown(KeyCode As Integer, Shift As Integer)

    Dim newIndex As Integer, iCount As Integer

    If ((KeyCode = vbKeyTab) And (Shift = 2)) Then
        KeyCode = 0
        Shift = 0
        
        If (TabStrip.SelectedItem.Index < TabStrip.Tabs.Count) Then
            newIndex = TabStrip.SelectedItem.Index + 1
        Else
            newIndex = 1
        End If
        
        TabStrip.SelectedItem.HighLighted = False
        TabStrip.Tabs.Item(newIndex).Selected = True
        TabStrip.SelectedItem.HighLighted = True
    End If

End Sub

Private Sub txtValue_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)

    If (Button <> vbKeyRButton) Then Exit Sub

    Dim nodeName As String, tabName As String
    
    nodeName = node.nodeName
    tabName = Selected.nodeName
    
    Select Case nodeName
        Case "City"
            mnuPopupCity.Enabled = False
            mnuPopupCityID.Enabled = False
            mnuPopupCityName.Enabled = False
            
            Select Case tabName
                Case "ID"
                    mnuPopupCity.Enabled = True
                    mnuPopupCityID.Enabled = True
                Case "Name"
                    mnuPopupCity.Enabled = True
                    mnuPopupCityName.Enabled = True
            End Select
            
            PopupMenu mnuPopupCity
        Case "Line"
            mnuPopupLineStatus.Enabled = False
            
            Select Case tabName
                Case "Status"
                    mnuPopupLineStatus.Enabled = True
            End Select
            
            PopupMenu mnuPopupLine
        Case "Station"
            mnuPopupStation.Enabled = False
            mnuPopupStationTransfer.Enabled = False
            mnuPopupStationStatus.Enabled = False
            mnuPopupStationDirection.Enabled = False
            mnuPopupStationReversible.Enabled = False
            mnuPopupStationBusImportTool.Enabled = False

            Select Case tabName
                Case "Status"
                    mnuPopupStation.Enabled = True
                    mnuPopupStationStatus.Enabled = True
                Case "Transfer"
                    mnuPopupStation.Enabled = True
                    mnuPopupStationTransfer.Enabled = True
                Case "Reversible"
                    mnuPopupStation.Enabled = True
                    mnuPopupStationReversible.Enabled = True
                Case "Direction"
                    mnuPopupStation.Enabled = True
                    mnuPopupStationDirection.Enabled = True
                Case "Bus"
                    mnuPopupStation.Enabled = True
                    mnuPopupStationBusImportTool.Enabled = True
            End Select
            
            PopupMenu mnuPopupStation
    End Select
            
End Sub

Private Sub UserControl_Initialize()

    Dim iCount As Integer

    TabStrip.Tabs.Clear
    TabStrip.Top = 0
    TabStrip.Left = 0

    CityIDs = Split(CityIDArray, ",")
    CityNames = Split(CityNameArray, ",")

    If (UBound(CityIDs) <> UBound(CityNames)) Then
        MsgBox "City data amount not match!", vbExclamation, App.Title
        
        Exit Sub
    Else
        mnuPopupCityIDItems(0).Caption = CityIDs(0)
        mnuPopupCityIDItems(0).Enabled = True
        mnuPopupCityNameItems(0).Caption = CityNames(0)
        mnuPopupCityNameItems(0).Enabled = True

        For iCount = 1 To UBound(CityIDs)
            Load mnuPopupCityIDItems(iCount)
            mnuPopupCityIDItems(iCount).Caption = CityIDs(iCount)
            mnuPopupCityIDItems(iCount).Enabled = True
            
            Load mnuPopupCityNameItems(iCount)
            mnuPopupCityNameItems(iCount).Caption = CityNames(iCount)
            mnuPopupCityNameItems(iCount).Enabled = True
        Next
    End If
    
    LineStatuses = Split(LineStatusArray, ",")
    mnuPopupLineStatusItems(0).Caption = LineStatuses(0)
    mnuPopupLineStatusItems(0).Enabled = True
    
    For iCount = 1 To UBound(LineStatuses)
        Load mnuPopupLineStatusItems(iCount)
        mnuPopupLineStatusItems(iCount).Caption = LineStatuses(iCount)
        mnuPopupLineStatusItems(iCount).Enabled = True
    Next
    
    StationStatuses = Split(StationStatusArray, ",")
    mnuPopupStationStatusItems(0).Caption = StationStatuses(0)
    mnuPopupStationStatusItems(0).Enabled = True
    
    For iCount = 1 To UBound(StationStatuses)
        Load mnuPopupStationStatusItems(iCount)
        mnuPopupStationStatusItems(iCount).Caption = StationStatuses(iCount)
        mnuPopupStationStatusItems(iCount).Enabled = True
    Next
    
    StationTransfers = Split(StationTransferArray, ",")
    mnuPopupStationTransferItems(0).Caption = StationTransfers(0)
    mnuPopupStationTransferItems(0).Enabled = True
    
    For iCount = 1 To UBound(StationTransfers)
        Load mnuPopupStationTransferItems(iCount)
        mnuPopupStationTransferItems(iCount).Caption = StationTransfers(iCount)
        mnuPopupStationTransferItems(iCount).Enabled = True
    Next
    
    StationReversibles = Split(StationReversibleArray, ",")
    mnuPopupStationReversibleItems(0).Caption = StationReversibles(0)
    mnuPopupStationReversibleItems(0).Enabled = True
    
    For iCount = 1 To UBound(StationReversibles)
        Load mnuPopupStationReversibleItems(iCount)
        mnuPopupStationReversibleItems(iCount).Caption = StationReversibles(iCount)
        mnuPopupStationReversibleItems(iCount).Enabled = True
    Next
    
    StationDirections = Split(StationDirectionArray, ",")
    mnuPopupStationDirectionItems(0).Caption = StationDirections(0)
    mnuPopupStationDirectionItems(0).Enabled = True
    
    For iCount = 1 To UBound(StationDirections)
        Load mnuPopupStationDirectionItems(iCount)
        mnuPopupStationDirectionItems(iCount).Caption = StationDirections(iCount)
        mnuPopupStationDirectionItems(iCount).Enabled = True
    Next
    
End Sub

Private Sub UserControl_Resize()

    If UserControl.Width > 0 Or UserControl.Height > 0 Then
        If UserControl.Width > 0 Then TabStrip.Width = UserControl.Width
        If UserControl.Height > 0 Then TabStrip.Height = UserControl.Height
        
        Call ResetTextBox
    End If

End Sub

Private Sub ResetTextBox()

    txtValue.Width = TabStrip.ClientWidth
    txtValue.Height = TabStrip.ClientHeight
    txtValue.Top = TabStrip.ClientTop
    txtValue.Left = TabStrip.ClientLeft

    If TabStrip.Tabs.Count = 0 Then
        txtValue.Visible = False
    Else
        txtValue.Visible = True
    End If

End Sub
